package by.krivenky.transnetmanagers;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        createDrawer(toolbar);
    }

    private void createDrawer(Toolbar toolbar) {
        AccountHeader accountHeader = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.colorHeader)
                .withSelectionListEnabledForSingleProfile(false)
                .addProfiles(
                        new ProfileDrawerItem().withName("Mike Penz").withEmail("mikepenz@gmail.com").withIcon(getResources().getDrawable(R.drawable.example_header))
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();

        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withIcon(R.drawable.list_fleet).withSelectedColorRes(R.color.colorDrawerItemBackground).withSelectedTextColorRes(R.color.colorDrawerItemTextActive).withTextColorRes(R.color.colorDrawerItemTextNotActive).withIdentifier(1).withName(R.string.list_of_my_fleet_menu_item);
        PrimaryDrawerItem item2 = new PrimaryDrawerItem().withIcon(R.drawable.map_fleet).withSelectedColorRes(R.color.colorDrawerItemBackground).withSelectedTextColorRes(R.color.colorDrawerItemTextActive).withTextColorRes(R.color.colorDrawerItemTextNotActive).withIdentifier(2).withName(R.string.my_fleet_on_the_map_menu_item);
        PrimaryDrawerItem item3 = new PrimaryDrawerItem().withIcon(R.drawable.orders).withSelectedColorRes(R.color.colorDrawerItemBackground).withSelectedTextColorRes(R.color.colorDrawerItemTextActive).withTextColorRes(R.color.colorDrawerItemTextNotActive).withIdentifier(3).withName(R.string.orders_menu_item);
        PrimaryDrawerItem item4 = new PrimaryDrawerItem().withIcon(R.drawable.notifications).withSelectedColorRes(R.color.colorDrawerItemBackground).withSelectedTextColorRes(R.color.colorDrawerItemTextActive).withTextColorRes(R.color.colorDrawerItemTextNotActive).withIdentifier(4).withName(R.string.notifications_menu_item);
        PrimaryDrawerItem item5 = new PrimaryDrawerItem().withIcon(R.drawable.messanger).withSelectedColorRes(R.color.colorDrawerItemBackground).withSelectedTextColorRes(R.color.colorDrawerItemTextActive).withTextColorRes(R.color.colorDrawerItemTextNotActive).withIdentifier(5).withName(R.string.messanger_menu_item);
        PrimaryDrawerItem item6 = new PrimaryDrawerItem().withIcon(R.drawable.settings).withSelectedColorRes(R.color.colorDrawerItemBackground).withSelectedTextColorRes(R.color.colorDrawerItemTextActive).withTextColorRes(R.color.colorDrawerItemTextNotActive).withIdentifier(6).withName(R.string.login_settings_menu_item);
        PrimaryDrawerItem item7 = new PrimaryDrawerItem().withIcon(R.drawable.signout).withSelectedColorRes(R.color.colorDrawerItemBackground).withSelectedTextColorRes(R.color.colorDrawerItemTextActive).withTextColorRes(R.color.colorDrawerItemTextNotActive).withIdentifier(7).withName(R.string.log_out_menu_item);

        Drawer result = new DrawerBuilder()
                .withActivity(this)
                .withAccountHeader(accountHeader)
                .withToolbar(toolbar)
                .withSliderBackgroundColorRes(R.color.colorDrawerItemBackground)
                .addDrawerItems(
                        item1,
                        item2,
                        item3,
                        item4,
                        item5,
                        item6,
                        item7
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        // do something with the clicked item :D

                        return false;
                    }
                })
                .build();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
